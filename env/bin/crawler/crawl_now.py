from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from crawler.auth import login_required
from crawler.db import get_db

import os
import sys
import glob
import json
import datetime

import scrapy
from scrapy.settings import Settings
from scrapy.crawler import CrawlerRunner, CrawlerProcess
from scrapy.utils.project import get_project_settings

import subprocess

from .crawler_api.crawler_api.spiders.feedley_spider import Feedly_Craler
from .crawler_api.crawler_api.crawler_run import CrawlerRun

bp = Blueprint('crawl_now', __name__)

@bp.route('/update', methods=('GET', 'POST'))
@login_required
def update():

    crawler_obj = CrawlerRun()
    last_update = crawler_obj.run()
    return render_template('blog/index.html', last_update=last_update)


@bp.route('/twitter', methods=('GET', 'POST'))
@login_required
def twitter():
    path = 'crawler/crawler_api/crawler_api/news/twitter/'
    files = sorted([os.path.join(path, file) for file in os.listdir(path)], key=os.path.getctime)
    news_list = json_reader(files)
    news_list = sorted(news_list, key=lambda k: k['ordered_time'], reverse=True)
    return render_template('display/twitter.html', posts=news_list[0:100])

@bp.route('/feedly', methods=('GET', 'POST'))
@login_required
def feedly():

    news_list = json_reader(['crawler/crawler_api/crawler_api/news/feedly/feedly.json'], True)
    cat_list = []
    news_list = sorted(news_list[0:500], key=lambda k: k['published'], reverse=True)

    for item in news_list:
        if not "categories" in item:
            item["categories"] = [{'label': 'Uncategorized', 'id': 'user/a1ab8f3a-8040-4e5b-802a-55c583f4abb1/category/global.uncategorized_tab_label'}]

        if item["categories"][0]['label'] not in cat_list:
            cat_list.append(item["categories"][0]['label'])

    return render_template('display/feedly.html', posts=news_list, categories=sorted(cat_list))


def json_reader(fname, feedly=False):
    list_items = []
    for name in fname:
        count = 1
        with open(name) as f:
            for line in f:
                # print("-----------------------------", file=sys.stderr)
                # print(line, file=sys.stderr)
                item = json.loads(line)
                if feedly:
                    """ Convert Seconds to Date"""
                    item['published'] = str(datetime.datetime.utcfromtimestamp(item['published']/1000).strftime('%Y-%m-%d %H:%M:%S'))
                list_items.append(item)
                if count == 1000:
                    break
                count+=1
    return list_items
