import json
import time
import scrapy

from crawler_api.scrapy_twitter import TwitterUserTimelineRequest, to_item


class UserTimelineSpider(scrapy.Spider):
    name = "pixelwolfhq"
    handle_httpstatus_list = [301, 302]
    allowed_domains = ["twitter.com"]

    def __init__(self, screen_name = None, *args, **kwargs):
        super(UserTimelineSpider, self).__init__(*args, **kwargs)
        self.screen_name = 'pixelwolfhq'
        self.count = 10

    def start_requests(self):
        return [ TwitterUserTimelineRequest(
                    screen_name = self.screen_name,
                    count = self.count)
                    ]

    def parse(self, response):
        tweets = response.tweets
        for tweet in tweets:

            if 'retweet_count' not in tweet:
                tweet.update({ "retweet_count":0})

            if 'favorite_count' not in tweet:
                tweet.update({ "favorite_count":0})

            item = to_item(tweet)
            res = {}

            if "in_reply_to_screen_name" not in tweet: #Use this to limit to only tweets and not retweets
                res['created_at'] = item['created_at']
                res['ordered_time'] = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(item['created_at'],'%a %b %d %H:%M:%S %Y'))
                res['text'] = item['text']
                res['user'] = item['user']
                res['retweet_count'] = item['retweet_count']
                res['favorite_count'] = item['favorite_count']
                res['urls'] = item['urls']

                with open('news/twitter/'+self.screen_name+'.json', 'a') as fout:
                    json.dump(dict(res), fout)
                    fout.write("\n")

        if tweets:
            yield TwitterUserTimelineRequest(
                    screen_name = self.screen_name,
                    count = self.count,
                    max_id = tweets[-1]['id'] - 1)
