import os
import sys
import json
import time
import requests


class Feedly_Craler(object):

	base_url = 'https://cloud.feedly.com'
	access_token = 'A2tlcsZkBDacWspUs-9uNfynBJw_V0JdIcALKPqy4NiNF3B0vwLc1R9R2Z__uvhdgsH6v0gg6uANYQDQQGtFldJa8dh_xEjLoa8Vdb7pFw4S5Nf1cEJ1CDIlwDn1_3P3tSl9IsYEPxdaG_EccPH-qsyJNl6LB8XuzhOVbqhqHAvdg4XJFoFqNshuC7lWFfpd4DOOFvSFglHiJEc3OGUFMb9HaLJC65zQYxQgRhqX0m77UkPRWSJFR4M:feedlydev'
	header = {'Authorization': 'OAuth ' + access_token}

	def refresh_token(self):
		function_url = "/v3/auth/token/"
		refresh_token = 'A2tlcsZkBDacWspUs-9uNfynBJw_V0JdIcALKPqy7NiNBCR36FXVhh4AxJf_vuRS3ob16FMu6eRYeQHdEGxNk4Zao4srzUbF6vsNbf2oBANO94fgNBUrHi4qnH76sG3_tCIuPMQGbhdbH6IdJqGrr92JNk2LB8Xt0ALZe6t5FQ7d0NSBB8R6PsB4EK1OH_oXpyDIbLyahlvrP0ExNSQIfP5HM7II65TRZA8sHA-P3CnVEkCUEGJMIcsgU5LyWIjvEg'
		client_id = 'feedlydev'
		client_secret = 'feedlydev'
		params = {
			"refresh_token"  : refresh_token,
			"client_id" : client_id,
			"client_secret" : client_secret,
			"grant_type" : "refresh_token"
		}
		response = requests.post(self.base_url+function_url, params=params).json()
		header = {'Authorization': 'OAuth ' + response.get('access_token')}
		return header

	def get_categories(self, header):
		function_url = '/v3/categories'
		response = requests.get(self.base_url+function_url, headers=header).json()

		if response.get('errorCode') == 429:
			time.sleep(10)
		if response.get('errorCode') == 401:
			new_header = self.refresh_token()
			response = requests.get(self.base_url+function_url, headers=new_header).json()

		list_of_categories = [item['label'] for item in response]
		list_of_categories.append("Uncategorized")
		return list_of_categories

	def get_user_subscriptions_by_category(self, filter_categories, header):
		function_url = '/v3/subscriptions'
		response = requests.get(self.base_url+function_url, headers=header).json()

		if response.get('errorCode') == 429:
			time.sleep(10)
		if response.get('errorCode') == 401:
			new_header = self.refresh_token()
			response = requests.get(self.base_url+function_url, headers=new_header).json()

		return [item['id'] for item in response if item.get('categories')[0].get('label') in filter_categories]

	def get_content(self, streamId_list, header):
		function_url = '/v3//streams/contents?streamId='

		for streamId in streamId_list:
			response = requests.get(self.base_url+function_url+streamId, headers=header).json()

			if response.get('errorCode') == 429:
				time.sleep(10)
			if response.get('errorCode') == 401:
				new_header = self.refresh_token()
				response = requests.get(self.base_url+function_url+streamId, headers=new_header).json()

			writepath = 'crawler/crawler_api/crawler_api/news/feedly/feedly.json'
			mode = 'a' if os.path.exists(writepath) else 'w'
			for item in response.get('items'):
				with open(writepath, mode) as fp:
					json.dump(dict(item), fp)
					fp.write("\n")

	def run_crawl(self):
		categories = self.get_categories(self.header)
		streamId_list = self.get_user_subscriptions_by_category(categories, self.header)
		self.get_content(streamId_list, self.header)
"""
A2tlcsZkBDacWspUs-9uNfynBJw_V0JdIcALKPqy4NiNF3B0vwLc1R9R2Z__uvhdgsH6v0gg6uANYQDQQGtFldJa8dh_xEjLoa8Vdb7pFw4S5Nf1cEJ1CDIlwDn1_3P3tSl9IsYEPxdaG_EccPH-qsyJNl6LB8XuzhOVbqhqHAvdg4XJFoFqNshuC7lWFfpd4DOOFvSFglHiJEc3OGUFMb9HaLJC65zQYxQgRhqX0m77UkPRWSJFR4M:feedlydev

refresh:
A2tlcsZkBDacWspUs-9uNfynBJw_V0JdIcALKPqy7NiNBCR36FXVhh4AxJf_vuRS3ob16FMu6eRYeQHdEGxNk4Zao4srzUbF6vsNbf2oBANO94fgNBUrHi4qnH76sG3_tCIuPMQGbhdbH6IdJqGrr92JNk2LB8Xt0ALZe6t5FQ7d0NSBB8R6PsB4EK1OH_oXpyDIbLyahlvrP0ExNSQIfP5HM7II65TRZA8sHA-P3CnVEkCUEGJMIcsgU5LyWIjvEg
"""
