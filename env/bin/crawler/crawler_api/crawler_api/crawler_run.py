import os
import sys
import scrapy
import subprocess
from time import gmtime, strftime
from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.utils.project import get_project_settings

from . import settings as my_settings
from .spiders.feedley_spider import Feedly_Craler

class CrawlerRun(object):

    def run(self):
        """ Feedly """
        process = CrawlerRunner({'USER_AGENT': "Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.6"})
        feedly = Feedly_Craler()
        feedly.run_crawl()
        """ Twitter """
        mydir = os.getcwd()
        mydir_tmp = os.path.dirname(os.path.abspath(__file__))
        os.chdir(mydir_tmp)
        subprocess.call(['scrapy', 'crawl', 'pixelwolfhq'])
        subprocess.call(['scrapy', 'crawl', 'jovemnerd'])
        subprocess.call(['scrapy', 'crawl', 'tec_mundo'])
        subprocess.call(['scrapy', 'crawl', 'realDonaldTrump'])
        subprocess.call(['scrapy', 'crawl', 'Curitiba_PMC'])
        os.chdir(mydir)
        return strftime("%Y-%m-%d %H:%M:%S", gmtime())
